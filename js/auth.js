//const { default: axios } = require('axios');
const buttons = function () {
    let bttns = document.querySelector('.buttons');
    bttns.hidden = false;
};

function onSignIn(googleUser) {
    let id_token = googleUser.getAuthResponse().id_token;
    axios
        .post(`/api/v1/auth/google-sign-in`, {
            idToken: id_token,
        })
        .then(function (response) {
            let accesToken = response.data.accessToken;
            // let refreshToken = response.data.refreshToken;
            axios
                .get(`/api/v1/auth/info`, {
                    headers: { Authorization: `Bearer ${accesToken}` },
                })
                .then(function (response) {
                    console.log(response);
                    buttons();
                });
        })
        .catch(function (error) {
            console.log(error);
        });
}
