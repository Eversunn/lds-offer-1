// const axios = require('axios').default;

const html = function (element, className, parent, innerHTML) {
    let name = className;
    className = document.createElement(`${element}`);
    className.className = `${name}`;
    className.innerHTML = `${innerHTML}`;
    document.querySelector(`.${parent}`).appendChild(className);
};

const valTest = function (val) {
    if (val.length > 18) {
        let newVal = `${val.substr(0, val.length - 18)}.${val.slice(
            val.length - 18
        )}`;
        console.log(newVal);
        return newVal;
    } else if (val.length == 18) {
        let newVal = `0.${val}`;
        console.log(newVal);
        return newVal;
    } else if (val.length < 18) {
        let newVal = val;
        for (let i = val.length; i < 18; i++) {
            newVal = `0${newVal}`;
        }
        newVal = `0.${newVal}`;
        console.log(newVal);
        return newVal;
    }
};

axios.get('/api/ledius-token/dispenser-account').then(response => {
    let data = response.data;
    console.log(data);
    html(`div`, `card`, `wrapper`, ``);
    html(`div`, `card_body`, `card`, ``);
    html(`div`, `account`, `card_body`, ``);
    html(`h2`, ``, `account`, `Account`);
    html(`P`, `account-id`, `account`, `ID - ${data.account.id}`);
    html(`P`, `account-userId`, `account`, `UserID - ${data.account.userId}`);
    html(
        `P`,
        `account-address`,
        `account`,
        `Address - ${data.account.address}`
    );
    html(`div`, `balances`, `card_body`, `<h2>Balances</h2>`);
    for (let i = 0; i < data.balances.length; i++) {
        let parent = document.createElement(`div`);
        parent.className = `balances_div`;
        document.querySelector(`.balances`).appendChild(parent);

        let creator = function (element, className, parent, innerHTML) {
            let name = className;
            className = document.createElement(`${element}`);
            className.className = `${name}`;
            className.innerHTML = `${innerHTML}`;
            parent.appendChild(className);
        };
        creator(
            `P`,
            `balances_balance`,
            parent,
            `balance - ${valTest(data.balances[i].balance)}`
        );
        creator(
            `P`,
            `balances_symbol`,
            parent,
            `symbol - ${data.balances[i].symbol}`
        );
        creator(
            `P`,
            `balances_decimals`,
            parent,
            `decimals - ${data.balances[i].decimals}`
        );
    }
});
