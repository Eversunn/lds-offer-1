// const axios = require('axios').default;
const wrapper = document.querySelector('.wrapper');

const offerStatus = ['OPENED', 'RESERVED', 'FULFILLED', 'CLOSED'];

const offerType = ['BUY', 'SELL'];

axios.get('/api/ledius-token/offers').then(response => {
    let data = response.data;
    for (let i = 0; i < data.items.length; i++) {
        let html = function (element, className, parent, innerHTML) {
            let name = className;
            className = document.createElement(`${element}`);
            className.className = `${name}`;
            className.innerHTML = `${innerHTML}`;
            parent.appendChild(className);
            return className;
        };

        let object = data.items[i];
        let card = html(`div`, `card`, wrapper, `<h1>Card - ${object.id}</h1>`);
        let cardBody = html(`div`, `card-body`, card, ``);

        let owner = html(`div`, `owner`, cardBody, `<p>Owner</p>`);
        html(`p`, `owner-id`, owner, `ID: ${object.owner.id}`);
        html(`p`, `owner-userId`, owner, `UserID: ${object.owner.userId}`);
        html(`p`, `owner-adress`, owner, `address: ${object.owner.address}`);

        let money = html(`div`, `money`, cardBody, `<p>Money</p>`);
        html(`p`, `amount`, money, `ammount: ${object.amount}`);
        let rate = html(`div`, `rate`, money, `<p>Rate</p>`);
        html(`p`, `currency`, rate, `currency: ${object.rate.currency}`);
        html(`p`, `price`, rate, `price: ${object.rate.price}`);

        let side = html(`div`, `side`, cardBody, `<p>side</p>`);
        let reserved = object.reservedBy;
        if (reserved == undefined) {
            html(`p`, `reserved`, side, `reserved by: ${reserved}`);
        } else {
            let reservDiv = html(`div`, `reserved`, side, `<p>Reserved by:<p>`);
            html(`p`, `reserved-id`, reservDiv, `ID: ${object.reservedBy.id}`);
            html(
                `p`,
                `reserved-userId`,
                reservDiv,
                `UserID: ${object.reservedBy.userId}`
            );
            html(
                `p`,
                `reserved-adress`,
                reservDiv,
                `address: ${object.reservedBy.address}`
            );
            cardBody.appendChild(reservDiv);
            cardBody.appendChild(side);
        }
        let bankDetails = object.bankDetails;
        if (bankDetails == undefined) {
            html(`p`, `bank`, side, `Bank Details - ${bankDetails}`);
        } else {
            let bankDiv = html(`div`, `bank`, side, `<p>Bank Details</p>`);
            html(`p`, `bank-id`, bankDiv, `ID: ${object.bankDetails.id}`);
            html('div', `bank-name`, bankDiv, ``);
            html(
                `p`,
                ``,
                bankDiv,
                `Name: ${object.bankDetails.name.first} ${object.bankDetails.name.patronymic} ${object.bankDetails.name.last}`
            );

            html(`p`, `bank-card`, bankDiv, `Card: ${object.bankDetails.card}`);
            html(`p`, `bank-bank`, bankDiv, `Bank: ${object.bankDetails.bank}`);
            html(
                `p`,
                `bank-UserId`,
                bankDiv,
                `UserID: ${object.bankDetails.userId}`
            );
            cardBody.appendChild(bankDiv);
            cardBody.appendChild(side);
        }
        html(`p`, ``, side, `publishedAt: ${object.publishedAt}`);
        html(`p`, ``, side, `createdAt: ${object.createdAt}`);
        html(`p`, ``, side, `updatedAt: ${object.updatedAt}`);

        let status = html(`div`, `status`, cardBody, `<p>status<p>`);
        html(`p`, ``, status, `status: ${offerStatus[object.status]}`);
        html(`p`, ``, status, `type: ${offerType[object.type]}`);
    }
});
