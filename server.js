const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
const path = require('path');

const app = express();

app.use(
    '/api',
    createProxyMiddleware({
        target: 'https://develop.ledius.ru',
        changeOrigin: true,
    })
);
app.use('/js', express.static(path.join(__dirname, '/js')));
app.use('/css', express.static(path.join(__dirname, '/css')));

const PORT = process.env.PORT || 80;

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index');
});
app.get('/offer', (req, res) => {
    res.render('offer');
});
app.get('/dispenser', (req, res) => {
    res.render('dispenser');
});

app.listen(PORT, () => {
    console.log(`server rinning on port ${PORT}`);
});
